% CLIGH(1) cligh Manual
% Christopher Brannon; William Hubbs
% 2014-08-29
# NAME

cligh - manage GitHub from console

# SYNOPSIS

cligh [-h] {configure,collab,issue,repo} [ARGS]

cligh configure [-h]

cligh collab [-h] {list,add,remove} [ARGS]

cligh issue [-h] {list,get,close,open,comment,add_label,remove_label} [ARGS]

cligh repo [-h] {list,create,fork,add_label,remove_label} [ARGS]

cligh collab list [-h] [--repository REPOSITORY]

cligh collab add [-h] [--repository REPOSITORY] USER

cligh collab remove [-h] [--repository REPOSITORY] USER

cligh issue list [-h] [--status STATUS] [--repository REPOSITORY]

cligh issue get [-h] [--repository REPOSITORY] NUMBER

cligh issue close [-h] [--repository REPOSITORY] NUMBER

cligh issue open [-h] [--repository REPOSITORY] TITLE

cligh issue add_label [-h] [--repository REPOSITORY] NUMBER LABEL

cligh issue remove_label [-h] [--repository REPOSITORY] NUMBER LABEL

cligh repo list [-h] USER

cligh repo create [-h]

cligh repo fork [-h] REPOSITORY

cligh repo add_label [-h] [--repository REPOSITORY] LABEL

cligh repo remove_label [-h] [--repository REPOSITORY] LABEL

# OPTIONS

-h,--help
:    show command or subcommand usage
--repository
:    repository to operate on in \<user\>/\<repository\> format

# NOTES

This manual page is written by Dmitry Bogatov for Debian project
but can be used by others.
